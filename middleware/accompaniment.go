package middleware

import (
	"net/http"
)

// AccompanimentFunc can invoke before and after request
type AccompanimentFunc func(w http.ResponseWriter, r *http.Request)

// NewAccompanimentBefore middleware invoke methods before main request
func NewAccompanimentBefore(before []AccompanimentFunc) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Run methods before source request.
			if before != nil {
				for _, f := range before {
					f(w, r)
				}
			}
			// Run source request.
			next.ServeHTTP(w, r)
		})
	}
}

// NewAccompanimentAfter middleware invoke methods after main request
func NewAccompanimentAfter(after []AccompanimentFunc) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Run source request.
			next.ServeHTTP(w, r)
			// Run methods after source request.
			if after != nil {
				for _, f := range after {
					f(w, r)
				}
			}
		})
	}
}
