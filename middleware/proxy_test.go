package middleware

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNewProxy(t *testing.T) {
	targetAResponse := "hello from targetA"
	targetServerA := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, targetAResponse)
	}))
	defer targetServerA.Close()
	targetBResponse := "hello from targetB"
	targetServerB := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, targetBResponse)
	}))
	defer targetServerB.Close()

	mockTargets := []Target{
		{Pattern: "/apiA", Url: targetServerA.URL},
		{Pattern: "/apiB", Url: targetServerB.URL},
	}

	proxy := NewProxy(mockTargets)
	proxyServer := httptest.NewServer(proxy(http.DefaultServeMux))
	defer proxyServer.Close()

	cases := []struct {
		name             string
		request          string
		expectedResponse string
	}{
		{
			name:             "good request A",
			request:          "/apiA/ping",
			expectedResponse: targetAResponse,
		},
		{
			name:             "good request B",
			request:          "/apiB/ping",
			expectedResponse: targetBResponse,
		},
		{
			name:             "bad request",
			request:          "/apiC/ping",
			expectedResponse: "page not found\n",
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			req := httptest.NewRequest(http.MethodGet, c.request, nil)
			resp := httptest.NewRecorder()
			proxyServer.Config.Handler.ServeHTTP(resp, req)
			assert.Equal(t, c.expectedResponse, resp.Body.String())
		})
	}

}
