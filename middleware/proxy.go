package middleware

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"net/url"
	"regexp"
)

// NewProxy is reverse proxy to targets.
func NewProxy(targets []Target) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Selected target with using regexp.
			var selected *Target

			// Select target.
			for _, target := range targets {
				// If regexp match - target found.
				if regexp.MustCompile(fmt.Sprintf("^%s.*", target.Pattern)).MatchString(r.URL.Path) {
					selected = &target
					break
				}
			}

			// If target not found - invoke http.StatusNotFound error for page.
			if selected == nil {
				http.Error(w, "page not found", http.StatusNotFound)
				return
			}

			// Cast selected target to url.Url.
			u, err := url.Parse(selected.Url)
			if err != nil {
				http.Error(w, "internal error", http.StatusInternalServerError)
				return
			}

			// Remove target pattern from source request url path.
			r.URL.Path = regexp.MustCompile(selected.Pattern).ReplaceAllString(r.URL.Path, "")

			// Create default reverse proxy based on selected target.
			proxy := httputil.NewSingleHostReverseProxy(u)

			// Serve source request with created proxy.
			proxy.ServeHTTP(w, r)
		})
	}
}
