package middleware

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestAccompanimentBeforeMiddleware(t *testing.T) {
	mockBeforeFunc1 := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-Mock-Before-1", "true")
	}

	mockBeforeFunc2 := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-Mock-Before-2", "true")
	}

	before := NewAccompanimentBefore([]AccompanimentFunc{mockBeforeFunc1, mockBeforeFunc2})

	testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Main Request Response"))
	})
	testServer := httptest.NewServer(before(testHandler))
	defer testServer.Close()

	req := httptest.NewRequest(http.MethodGet, testServer.URL, nil)
	res := httptest.NewRecorder()
	testServer.Config.Handler.ServeHTTP(res, req)

	if status := res.Code; status != http.StatusOK {
		t.Errorf("Expected status %d, got %d", http.StatusOK, status)
	}

	if value := res.Header().Get("X-Mock-Before-1"); value != "true" {
		t.Errorf("Expected header 'X-Mock-Before-1: true', got '%s'", value)
	}

	if value := res.Header().Get("X-Mock-Before-2"); value != "true" {
		t.Errorf("Expected header 'X-Mock-Before-2: true', got '%s'", value)
	}

	// Check the main request response.
	if body := res.Body.String(); body != "Main Request Response" {
		t.Errorf("Expected response 'Main Request Response', got '%s'", body)
	}
}

func TestAccompanimentAfterMiddleware(t *testing.T) {
	mockAfterFunc1 := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-Mock-After-1", "true")
	}

	mockAfterFunc2 := func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("X-Mock-After-2", "true")
	}

	after := NewAccompanimentAfter([]AccompanimentFunc{mockAfterFunc1, mockAfterFunc2})

	testHandler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Main Request Response"))
	})
	testServer := httptest.NewServer(after(testHandler))
	defer testServer.Close()

	req := httptest.NewRequest(http.MethodGet, testServer.URL, nil)
	res := httptest.NewRecorder()
	testServer.Config.Handler.ServeHTTP(res, req)

	if status := res.Code; status != http.StatusOK {
		t.Errorf("Expected status %d, got %d", http.StatusOK, status)
	}

	if body := res.Body.String(); body != "Main Request Response" {
		t.Errorf("Expected response 'Main Request Response', got '%s'", body)
	}

	if value := res.Header().Get("X-Mock-After-1"); value != "true" {
		t.Errorf("Expected header 'X-Mock-After-1: true', got '%s'", value)
	}

	if value := res.Header().Get("X-Mock-After-2"); value != "true" {
		t.Errorf("Expected header 'X-Mock-After-2: true', got '%s'", value)
	}
}
