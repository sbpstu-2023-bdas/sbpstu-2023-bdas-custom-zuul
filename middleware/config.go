package middleware

type Target struct {
	Pattern string `yaml:"pattern"`
	Url     string `yaml:"url"`
}
