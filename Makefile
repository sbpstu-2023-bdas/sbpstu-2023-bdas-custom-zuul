.PHONY: deps
deps:
	go mod tidy

.PHONY: test
test: deps
	go test -cover ./...